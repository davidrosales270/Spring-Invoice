INSERT INTO client (name, lastname, email, create_at, photo) VALUES('David', 'Rosales', 'davidrosales270@gmail.com', '2018-01-06','');
INSERT INTO client (name, lastname, email, create_at, photo) VALUES('Matias', 'Rosales', 'matiasrosales270@gmail.com', '2018-01-06','');
INSERT INTO client (name, lastname, email, create_at, photo) VALUES('Dante', 'Rosales', 'danterosales270@gmail.com', '2018-01-06','');
INSERT INTO client (name, lastname, email, create_at, photo) VALUES('Brenda', 'Burgos', 'brendaburgosjimenez@gmail.com', '2018-01-06','');
INSERT INTO client (name, lastname, email, create_at, photo) VALUES('Xavier', 'Burgos', 'xavierburgos@gmail.com', '2018-01-06','');
INSERT INTO client (name, lastname, email, create_at, photo) VALUES('Neftali', 'Rosales', 'neftalirosales@gmail.com', '2018-01-06','');

INSERT INTO products (name, price, create_at) VALUES('Panasonic Pantalla LCD', 259990, NOW());
INSERT INTO products (name, price, create_at) VALUES('Sony Camara Digital DSC-W320B', 123490, NOW());
INSERT INTO products (name, price, create_at) VALUES('Apple Ipod shuffle', 1499990, NOW());
INSERT INTO products (name, price, create_at) VALUES('Sony Camara Digital DSC-W320B', 123490, NOW());
INSERT INTO products (name, price, create_at) VALUES('Sony Netbook Z110', 37990, NOW());
INSERT INTO products (name, price, create_at) VALUES('Hewlett Packard Multifuncional F2280', 69990, NOW());
INSERT INTO products (name, price, create_at) VALUES('Biachi Bicicleta Aro 26', 69990, NOW());
INSERT INTO products (name, price, create_at) VALUES('Mica Comoda 5 Cajones', 299990, NOW());

INSERT INTO invoices(description, observation, client_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());
INSERT INTO invoices_items(quantity, invoice_id, product_id) VALUES(1,1,1);
INSERT INTO invoices_items(quantity, invoice_id, product_id) VALUES(2,1,4);
INSERT INTO invoices_items(quantity, invoice_id, product_id) VALUES(1,1,5);
INSERT INTO invoices_items(quantity, invoice_id, product_id) VALUES(1,1,7);

INSERT INTO invoices(description, observation, client_id, create_at) VALUES('Factura Bicicleta', 'Alguna nota importante', 1, NOW());
INSERT INTO invoices_items(quantity, invoice_id, product_id) VALUES(3,2,6);